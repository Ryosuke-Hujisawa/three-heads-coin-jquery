# three-heads-coin-jquery

コイントスをすると二分の一の確率で表が出ますが、時として三回とも表が出ることもある。裏だったはずなのに表に引っくり返っることもある。これはスリーヘッドコイン(1/8)というイディオムをアニメーションで表現したものです。時として人生は奇妙でロマンチックなものなのです。

If you do coin toss, the heads comes out with a probability of a half, but sometimes the heads comes out three times. Although it should have been the back, it sometimes returns to the heads. This is an animation of an idiom called Three Head Coin (1/8). Sometimes life is strange and romantic.

## Demo

![Alt Text](http://wordtranslate.info/img/3hreadscoin.gif)




## Requirement

- jQuery 
- HTML5
- CSS3

## Usage

http://wordtranslate.info/three-heads-coin-jquery/three_heads_coin.html

## Licence

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)

## Author

[ryosuke-hujisawa](https://github.com/ryosuke-hujisawa)