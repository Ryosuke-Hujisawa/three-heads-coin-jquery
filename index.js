jQuery(document).ready(function($){
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

  $('#coin').on('click', function(){

    var flipResult = getRandomInt(2);

    $('#coin').removeClass();

    setTimeout(function(){

      if(flipResult == 0){

        $('#coin').addClass('heads');

        console.log('it is head');

              setTimeout(function(){
      $('body').append(`
      <div id="wrap"><h1 id="title">Congrats!</h1></div>
      <style>
      html, body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        font-size: 10px;
        overflow: hidden;
      }
      @media screen and (max-width: 1024px) {
        html, body {
          font-size: 7px;
        }
      }
      @media screen and (max-width: 768px) {
        html, body {
          font-size: 5px;
        }
      }
      @media screen and (max-width: 500px) {
        html, body {
          font-size: 4px;
        }
      }
      body {
        background: #000;
        background: -webkit-radial-gradient(#222, #000);
        background: -o-radial-gradient(#222, #000);
        background: -moz-radial-gradient(#222, #000);
        background: radial-gradient(#222, #000);
      }
      #wrap {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        height: 100%;
      }
      h1 {
        font: bold 20em/1 "Arial Black", Arial, serif;
        animation: title 1000ms linear 0s infinite;
        text-align: center;
        z-index: 100;
      }
      .firework {
        position: absolute;
      }
      .firework i {
        position: absolute;
        height: 0.5em;
        width: 0.5em;
        top: 0;
        left: 0;
        border-radius: 50%;
        opacity: 0;
        -webkit-box-shadow: 0 0 24px #ffc;
      }
      .firework i:nth-child(1) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(2) {
        animation: flare-20 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(3) {
        animation: flare-15 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(4) {
        animation: flare-10 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(5) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(6) {
        animation: flare-21 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(7) {
        animation: flare-10 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(8) {
        animation: flare-9 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(9) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(10) {
        animation: flare-13 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(11) {
        animation: flare-25 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(12) {
        animation: flare-5 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(13) {
        animation: flare-2 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(14) {
        animation: flare-4 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(15) {
        animation: flare-10 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(16) {
        animation: flare-7 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(17) {
        animation: flare-13 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(18) {
        animation: flare-19 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(19) {
        animation: flare-14 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(20) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(21) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(22) {
        animation: flare-10 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(23) {
        animation: flare-20 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(24) {
        animation: flare-2 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(25) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }

      .firework:nth-child(0) {
        content: "0";
        top: 30%;
        left: 29%;
      }
      .firework:nth-child(0) i {
        background-color: #ff24ff;
        animation-delay: 0ms;
      }

      .firework:nth-child(1) {
        content: "1";
        top: 25%;
        left: 42%;
      }
      .firework:nth-child(1) i {
        background-color: #a3ffff;
        animation-delay: 925ms;
      }

      .firework:nth-child(2) {
        content: "2";
        top: 20%;
        left: 56%;
      }
      .firework:nth-child(2) i {
        background-color: #ffa3ff;
        animation-delay: 1118ms;
      }

      .firework:nth-child(3) {
        content: "3";
        top: 47%;
        left: 43%;
      }
      .firework:nth-child(3) i {
        background-color: #d1ffa3;
        animation-delay: 2838ms;
      }

      .firework:nth-child(4) {
        content: "4";
        top: 26%;
        left: 33%;
      }
      .firework:nth-child(4) i {
        background-color: #ffa3ff;
        animation-delay: 2000ms;
      }

      .firework:nth-child(5) {
        content: "5";
        top: 35%;
        left: 24%;
      }
      .firework:nth-child(5) i {
        background-color: #ffdfa3;
        animation-delay: 3000ms;
      }

      .firework:nth-child(6) {
        content: "6";
        top: 37%;
        left: 65%;
      }
      .firework:nth-child(6) i {
        background-color: white;
        animation-delay: 4500ms;
      }

      .firework:nth-child(7) {
        content: "7";
        top: 3%;
        left: 69%;
      }
      .firework:nth-child(7) i {
        background-color: #ffffa3;
        animation-delay: 2408ms;
      }

      .firework:nth-child(8) {
        content: "8";
        top: 26%;
        left: 32%;
      }
      .firework:nth-child(8) i {
        background-color: #ffffa3;
        animation-delay: 4840ms;
      }

      .firework:nth-child(9) {
        content: "9";
        top: 69%;
        left: 54%;
      }
      .firework:nth-child(9) i {
        background-color: #ffdfa3;
        animation-delay: 6048ms;
      }

      .firework:nth-child(10) {
        content: "10";
        top: 37%;
        left: 30%;
      }
      .firework:nth-child(10) i {
        background-color: #ffdfa3;
        animation-delay: 1380ms;
      }

      @keyframes flare-1 {
        0% {
          opacity: 0;
        }
        33% {
          left: -7px;
          top: 4px;
          opacity: 0.6;
        }
        100% {
          left: -7px;
          top: 67px;
          opacity: 0;
        }
      }
      @keyframes flare-2 {
        0% {
          opacity: 0;
        }
        33% {
          left: 14px;
          top: 6px;
          opacity: 0.6;
        }
        100% {
          left: 14px;
          top: 88px;
          opacity: 0;
        }
      }
      @keyframes flare-3 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: -9px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 12px;
          opacity: 0;
        }
      }
      @keyframes flare-4 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: -8px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 38px;
          opacity: 0;
        }
      }
      @keyframes flare-5 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: 30px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 102px;
          opacity: 0;
        }
      }
      @keyframes flare-6 {
        0% {
          opacity: 0;
        }
        33% {
          left: -60px;
          top: 30px;
          opacity: 0.6;
        }
        100% {
          left: -60px;
          top: 75px;
          opacity: 0;
        }
      }
      @keyframes flare-7 {
        0% {
          opacity: 0;
        }
        33% {
          left: -70px;
          top: -42px;
          opacity: 0.6;
        }
        100% {
          left: -70px;
          top: 27px;
          opacity: 0;
        }
      }
      @keyframes flare-8 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: 48px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 89px;
          opacity: 0;
        }
      }
      @keyframes flare-9 {
        0% {
          opacity: 0;
        }
        33% {
          left: -81px;
          top: -45px;
          opacity: 0.6;
        }
        100% {
          left: -81px;
          top: 49px;
          opacity: 0;
        }
      }
      @keyframes flare-10 {
        0% {
          opacity: 0;
        }
        33% {
          left: 200px;
          top: 20px;
          opacity: 0.6;
        }
        100% {
          left: 200px;
          top: 80px;
          opacity: 0;
        }
      }
      @keyframes flare-11 {
        0% {
          opacity: 0;
        }
        33% {
          left: 88px;
          top: -55px;
          opacity: 0.6;
        }
        100% {
          left: 88px;
          top: 42px;
          opacity: 0;
        }
      }
      @keyframes flare-12 {
        0% {
          opacity: 0;
        }
        33% {
          left: 84px;
          top: 60px;
          opacity: 0.6;
        }
        100% {
          left: 84px;
          top: 129px;
          opacity: 0;
        }
      }
      @keyframes flare-13 {
        0% {
          opacity: 0;
        }
        33% {
          left: -130px;
          top: 91px;
          opacity: 0.6;
        }
        100% {
          left: -130px;
          top: 167px;
          opacity: 0;
        }
      }
      @keyframes flare-14 {
        0% {
          opacity: 0;
        }
        33% {
          left: 196px;
          top: 84px;
          opacity: 0.6;
        }
        100% {
          left: 196px;
          top: 126px;
          opacity: 0;
        }
      }
      @keyframes flare-15 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: -75px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: -33px;
          opacity: 0;
        }
      }
      @keyframes flare-16 {
        0% {
          opacity: 0;
        }
        33% {
          left: 96px;
          top: 0px;
          opacity: 0.6;
        }
        100% {
          left: 96px;
          top: 20px;
          opacity: 0;
        }
      }
      @keyframes flare-17 {
        0% {
          opacity: 0;
        }
        33% {
          left: 272px;
          top: 68px;
          opacity: 0.6;
        }
        100% {
          left: 272px;
          top: 130px;
          opacity: 0;
        }
      }
      @keyframes flare-18 {
        0% {
          opacity: 0;
        }
        33% {
          left: 324px;
          top: 0px;
          opacity: 0.6;
        }
        100% {
          left: 324px;
          top: 88px;
          opacity: 0;
        }
      }
      @keyframes flare-19 {
        0% {
          opacity: 0;
        }
        33% {
          left: 266px;
          top: 152px;
          opacity: 0.6;
        }
        100% {
          left: 266px;
          top: 170px;
          opacity: 0;
        }
      }
      @keyframes flare-20 {
        0% {
          opacity: 0;
        }
        33% {
          left: 140px;
          top: -140px;
          opacity: 0.6;
        }
        100% {
          left: 140px;
          top: -75px;
          opacity: 0;
        }
      }
      @keyframes flare-21 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: 63px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 149px;
          opacity: 0;
        }
      }
      @keyframes flare-22 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: -88px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 11px;
          opacity: 0;
        }
      }
      @keyframes flare-23 {
        0% {
          opacity: 0;
        }
        33% {
          left: 161px;
          top: 46px;
          opacity: 0.6;
        }
        100% {
          left: 161px;
          top: 68px;
          opacity: 0;
        }
      }
      @keyframes flare-24 {
        0% {
          opacity: 0;
        }
        33% {
          left: -192px;
          top: 48px;
          opacity: 0.6;
        }
        100% {
          left: -192px;
          top: 114px;
          opacity: 0;
        }
      }
      @keyframes flare-25 {
        0% {
          opacity: 0;
        }
        33% {
          left: 450px;
          top: 125px;
          opacity: 0.6;
        }
        100% {
          left: 450px;
          top: 134px;
          opacity: 0;
        }
      }
      @keyframes title {
        0% {
          color: #f00;
        }
        10% {
          color: #ff0;
        }
        20% {
          color: #fff;
        }
        30% {
          color: #0f0;
        }
        40% {
          color: #ff0;
        }
        50% {
          color: #0ff;
        }
        60% {
          color: #fff;
        }
        70% {
          color: #00f;
        }
        80% {
          color: #f0f;
        }
        90% {
          color: #ff0;
        }
        100% {
          color: #fff;
        }
      }
      </style>`);},3500);
      setTimeout(function(){  location.reload();  },7000);












      }else{

      $('#coin').addClass('tails');

      console.log('it is tails');

      setTimeout(function(){$('#coin').addClass('headsheads');},4000);

      setTimeout(function(){
      $('body').append(`
      <div id="wrap"><h1 id="title">Congrats!</h1></div>
      <style>
      html, body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        font-size: 10px;
        overflow: hidden;
      }
      @media screen and (max-width: 1024px) {
        html, body {
          font-size: 7px;
        }
      }
      @media screen and (max-width: 768px) {
        html, body {
          font-size: 5px;
        }
      }
      @media screen and (max-width: 500px) {
        html, body {
          font-size: 4px;
        }
      }
      body {
        background: #000;
        background: -webkit-radial-gradient(#222, #000);
        background: -o-radial-gradient(#222, #000);
        background: -moz-radial-gradient(#222, #000);
        background: radial-gradient(#222, #000);
      }
      #wrap {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        height: 100%;
      }
      h1 {
        font: bold 20em/1 "Arial Black", Arial, serif;
        animation: title 1000ms linear 0s infinite;
        text-align: center;
        z-index: 100;
      }
      .firework {
        position: absolute;
      }
      .firework i {
        position: absolute;
        height: 0.5em;
        width: 0.5em;
        top: 0;
        left: 0;
        border-radius: 50%;
        opacity: 0;
        -webkit-box-shadow: 0 0 24px #ffc;
      }
      .firework i:nth-child(1) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(2) {
        animation: flare-20 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(3) {
        animation: flare-15 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(4) {
        animation: flare-10 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(5) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(6) {
        animation: flare-21 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(7) {
        animation: flare-10 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(8) {
        animation: flare-9 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(9) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(10) {
        animation: flare-13 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(11) {
        animation: flare-25 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(12) {
        animation: flare-5 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(13) {
        animation: flare-2 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(14) {
        animation: flare-4 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(15) {
        animation: flare-10 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(16) {
        animation: flare-7 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(17) {
        animation: flare-13 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(18) {
        animation: flare-19 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(19) {
        animation: flare-14 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(20) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(21) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(22) {
        animation: flare-10 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(23) {
        animation: flare-20 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(24) {
        animation: flare-2 1000ms ease-out infinite forwards;
      }
      .firework i:nth-child(25) {
        animation: flare-12 1000ms ease-out infinite forwards;
      }

      .firework:nth-child(0) {
        content: "0";
        top: 30%;
        left: 29%;
      }
      .firework:nth-child(0) i {
        background-color: #ff24ff;
        animation-delay: 0ms;
      }

      .firework:nth-child(1) {
        content: "1";
        top: 25%;
        left: 42%;
      }
      .firework:nth-child(1) i {
        background-color: #a3ffff;
        animation-delay: 925ms;
      }

      .firework:nth-child(2) {
        content: "2";
        top: 20%;
        left: 56%;
      }
      .firework:nth-child(2) i {
        background-color: #ffa3ff;
        animation-delay: 1118ms;
      }

      .firework:nth-child(3) {
        content: "3";
        top: 47%;
        left: 43%;
      }
      .firework:nth-child(3) i {
        background-color: #d1ffa3;
        animation-delay: 2838ms;
      }

      .firework:nth-child(4) {
        content: "4";
        top: 26%;
        left: 33%;
      }
      .firework:nth-child(4) i {
        background-color: #ffa3ff;
        animation-delay: 2000ms;
      }

      .firework:nth-child(5) {
        content: "5";
        top: 35%;
        left: 24%;
      }
      .firework:nth-child(5) i {
        background-color: #ffdfa3;
        animation-delay: 3000ms;
      }

      .firework:nth-child(6) {
        content: "6";
        top: 37%;
        left: 65%;
      }
      .firework:nth-child(6) i {
        background-color: white;
        animation-delay: 4500ms;
      }

      .firework:nth-child(7) {
        content: "7";
        top: 3%;
        left: 69%;
      }
      .firework:nth-child(7) i {
        background-color: #ffffa3;
        animation-delay: 2408ms;
      }

      .firework:nth-child(8) {
        content: "8";
        top: 26%;
        left: 32%;
      }
      .firework:nth-child(8) i {
        background-color: #ffffa3;
        animation-delay: 4840ms;
      }

      .firework:nth-child(9) {
        content: "9";
        top: 69%;
        left: 54%;
      }
      .firework:nth-child(9) i {
        background-color: #ffdfa3;
        animation-delay: 6048ms;
      }

      .firework:nth-child(10) {
        content: "10";
        top: 37%;
        left: 30%;
      }
      .firework:nth-child(10) i {
        background-color: #ffdfa3;
        animation-delay: 1380ms;
      }

      @keyframes flare-1 {
        0% {
          opacity: 0;
        }
        33% {
          left: -7px;
          top: 4px;
          opacity: 0.6;
        }
        100% {
          left: -7px;
          top: 67px;
          opacity: 0;
        }
      }
      @keyframes flare-2 {
        0% {
          opacity: 0;
        }
        33% {
          left: 14px;
          top: 6px;
          opacity: 0.6;
        }
        100% {
          left: 14px;
          top: 88px;
          opacity: 0;
        }
      }
      @keyframes flare-3 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: -9px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 12px;
          opacity: 0;
        }
      }
      @keyframes flare-4 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: -8px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 38px;
          opacity: 0;
        }
      }
      @keyframes flare-5 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: 30px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 102px;
          opacity: 0;
        }
      }
      @keyframes flare-6 {
        0% {
          opacity: 0;
        }
        33% {
          left: -60px;
          top: 30px;
          opacity: 0.6;
        }
        100% {
          left: -60px;
          top: 75px;
          opacity: 0;
        }
      }
      @keyframes flare-7 {
        0% {
          opacity: 0;
        }
        33% {
          left: -70px;
          top: -42px;
          opacity: 0.6;
        }
        100% {
          left: -70px;
          top: 27px;
          opacity: 0;
        }
      }
      @keyframes flare-8 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: 48px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 89px;
          opacity: 0;
        }
      }
      @keyframes flare-9 {
        0% {
          opacity: 0;
        }
        33% {
          left: -81px;
          top: -45px;
          opacity: 0.6;
        }
        100% {
          left: -81px;
          top: 49px;
          opacity: 0;
        }
      }
      @keyframes flare-10 {
        0% {
          opacity: 0;
        }
        33% {
          left: 200px;
          top: 20px;
          opacity: 0.6;
        }
        100% {
          left: 200px;
          top: 80px;
          opacity: 0;
        }
      }
      @keyframes flare-11 {
        0% {
          opacity: 0;
        }
        33% {
          left: 88px;
          top: -55px;
          opacity: 0.6;
        }
        100% {
          left: 88px;
          top: 42px;
          opacity: 0;
        }
      }
      @keyframes flare-12 {
        0% {
          opacity: 0;
        }
        33% {
          left: 84px;
          top: 60px;
          opacity: 0.6;
        }
        100% {
          left: 84px;
          top: 129px;
          opacity: 0;
        }
      }
      @keyframes flare-13 {
        0% {
          opacity: 0;
        }
        33% {
          left: -130px;
          top: 91px;
          opacity: 0.6;
        }
        100% {
          left: -130px;
          top: 167px;
          opacity: 0;
        }
      }
      @keyframes flare-14 {
        0% {
          opacity: 0;
        }
        33% {
          left: 196px;
          top: 84px;
          opacity: 0.6;
        }
        100% {
          left: 196px;
          top: 126px;
          opacity: 0;
        }
      }
      @keyframes flare-15 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: -75px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: -33px;
          opacity: 0;
        }
      }
      @keyframes flare-16 {
        0% {
          opacity: 0;
        }
        33% {
          left: 96px;
          top: 0px;
          opacity: 0.6;
        }
        100% {
          left: 96px;
          top: 20px;
          opacity: 0;
        }
      }
      @keyframes flare-17 {
        0% {
          opacity: 0;
        }
        33% {
          left: 272px;
          top: 68px;
          opacity: 0.6;
        }
        100% {
          left: 272px;
          top: 130px;
          opacity: 0;
        }
      }
      @keyframes flare-18 {
        0% {
          opacity: 0;
        }
        33% {
          left: 324px;
          top: 0px;
          opacity: 0.6;
        }
        100% {
          left: 324px;
          top: 88px;
          opacity: 0;
        }
      }
      @keyframes flare-19 {
        0% {
          opacity: 0;
        }
        33% {
          left: 266px;
          top: 152px;
          opacity: 0.6;
        }
        100% {
          left: 266px;
          top: 170px;
          opacity: 0;
        }
      }
      @keyframes flare-20 {
        0% {
          opacity: 0;
        }
        33% {
          left: 140px;
          top: -140px;
          opacity: 0.6;
        }
        100% {
          left: 140px;
          top: -75px;
          opacity: 0;
        }
      }
      @keyframes flare-21 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: 63px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 149px;
          opacity: 0;
        }
      }
      @keyframes flare-22 {
        0% {
          opacity: 0;
        }
        33% {
          left: 0px;
          top: -88px;
          opacity: 0.6;
        }
        100% {
          left: 0px;
          top: 11px;
          opacity: 0;
        }
      }
      @keyframes flare-23 {
        0% {
          opacity: 0;
        }
        33% {
          left: 161px;
          top: 46px;
          opacity: 0.6;
        }
        100% {
          left: 161px;
          top: 68px;
          opacity: 0;
        }
      }
      @keyframes flare-24 {
        0% {
          opacity: 0;
        }
        33% {
          left: -192px;
          top: 48px;
          opacity: 0.6;
        }
        100% {
          left: -192px;
          top: 114px;
          opacity: 0;
        }
      }
      @keyframes flare-25 {
        0% {
          opacity: 0;
        }
        33% {
          left: 450px;
          top: 125px;
          opacity: 0.6;
        }
        100% {
          left: 450px;
          top: 134px;
          opacity: 0;
        }
      }
      @keyframes title {
        0% {
          color: #f00;
        }
        10% {
          color: #ff0;
        }
        20% {
          color: #fff;
        }
        30% {
          color: #0f0;
        }
        40% {
          color: #ff0;
        }
        50% {
          color: #0ff;
        }
        60% {
          color: #fff;
        }
        70% {
          color: #00f;
        }
        80% {
          color: #f0f;
        }
        90% {
          color: #ff0;
        }
        100% {
          color: #fff;
        }
      }
      </style>`);},7000);
      setTimeout(function(){  location.reload();  },10000);

      }
    }, 100);
  });
});





var FIREWORK_COUNT = 10,
    FLARES_COUNT = 25,
    hash = '';
document.open();
document.write('<div class="wrap">');
for (var i = 0; i < FIREWORK_COUNT; i++) {
  document.write('<div class="firework">');
  for (var j = 0; j < FLARES_COUNT; j++) {
    document.write('<i></i>');
  }
  document.write('</div>');
}
document.write('</div>');
document.close();

hash = window.location.hash.replace('#', '');
if (hash !== '') {
  document.getElementById('title').innerText = decodeURIComponent(hash);
  document.title = decodeURIComponent(hash);
}